package com.kyc.kycapp.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.kyc.kycapp.appconstant.AppConstant;


@Entity
@Table( name=AppConstant.COUNTRY_INFO)

public class Country implements Serializable {
	@Id
	@GenericGenerator(name = "m_auto",strategy="increment")
	@GeneratedValue(generator = "m_auto")
	
	@Column(name="id")
	private Long id;
	@Column(name="name")
	private String name;
	
	@Column(name="population")
	private  Long population;
	
	@Column(name="capital")
	private String capital;
	
	@Column(name="area")
	private  Long area;
	
	@Column(name="continent")
	private  String continent;
	
	@Column(name="no_of_state")
	private  String noOfstate;
	
	@OneToOne(cascade =CascadeType.ALL)
	@JoinColumn(name = "fro_key")
	
	private PrimeMinister primeminister;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public Long getArea() {
		return area;
	}

	public void setArea(Long area) {
		this.area = area;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public String getNoOfstate() {
		return noOfstate;
	}

	public void setNoOfstate(String noOfstate) {
		this.noOfstate = noOfstate;
	}

	public PrimeMinister getPrimeminister() {
		return primeminister;
	}

	public void setPrimeminister(PrimeMinister primeminister) {
		this.primeminister = primeminister;
	}
	
	

}
