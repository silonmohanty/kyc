package com.kyc.kycapp.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

	public class Utility {
	private static SessionFactory s=null;
	private Utility()
	{
		
	}
	public static SessionFactory getInstance()
	{
		if(s==null)
		{
//			Configuration cfg = new Configuration();
//			cfg.configure();
			s = new Configuration().configure().buildSessionFactory();
		}
		return s;
	}
}

