package com.kyc.kycapp.repository;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.kyc.kycapp.entity.Document;
import com.kyc.kycapp.util.Utility;
  
public class KycRepository {
	public void saveDocumentdetails(Document document)
	{
		try {
			
			SessionFactory ins = Utility.getInstance();
			
//			Configuration cfg = new Configuration();
//			cfg.configure();
//			SessionFactory sessionFactory = cfg.buildSessionFactory();
			
			Session session = ins.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(document);
			transaction.commit();
			
			
		} catch (HibernateException e) {
			
		}
			
		
	}

	
	public Document getDocumentById(Long Id) {
		
		SessionFactory ins = Utility.getInstance();
//		Configuration cfg = new Configuration();
//		cfg.configure();
//		SessionFactory sessionFactory= cfg.buildSessionFactory();
	    Session session = ins.openSession();
		Document document = session.get(Document.class, Id);
		return document;
		
	
		}
		
		
		
	 public void  updatingContactnumberById(Long contactnumber,Long Id ) 
	 {
		 SessionFactory ins = Utility.getInstance();
		  Document document= getDocumentById(Id);
	  if( document ==null)
		 System.out.println("Kyc not found");
	  else
	  {
			  document.setContactnumber(contactnumber);
//		  Configuration cfg = new Configuration();
//			cfg.configure();
//			SessionFactory sessionFactory= cfg.buildSessionFactory();
			Session session = ins.openSession();
			Transaction transaction = session.beginTransaction();
			session.merge(document);
			transaction.commit();  
			System.out.println("update success");
	  } 	
	  }

	 public void  deleteDocumentById(Long Id)
		{
		 SessionFactory ins = Utility.getInstance();
//		   Configuration cfg  =new Configuration();
//		   cfg.configure();
//		   SessionFactory sessionFactory = cfg.buildSessionFactory();
		   Session session = ins.openSession();
		   Document document = session.get(Document.class, Id);
		   if(document==null)
		   {
			   
			   System.out.println("There is no data inside movie_info");
		   }
		   else
		   {
			   session.beginTransaction();
			   session.delete(document);
			   session.getTransaction().commit();
			   
			   System.out.println("There is some data  so deleted  sucessfully");
		   }	
		}
 
	  public List<Document> findAll()
	  {
		  SessionFactory ins = Utility.getInstance();
//	          Configuration cfg =new  Configuration();
//	          cfg.configure();
//	          SessionFactory sessionFactory =cfg.buildSessionFactory();
	          Session sesion  =ins.openSession();
	          String hql="from Document";
	          Query query = sesion.createQuery(hql);
	          return query.list();
	  }
	  public List<Document> findAllByDocumentType(String documenttype)
	  {
		  SessionFactory ins = Utility.getInstance();
//	          Configuration cfg =new  Configuration();
//	          cfg.configure();
//	          SessionFactory sessionFactory =cfg.buildSessionFactory();
	          Session sesion  =ins.openSession();
	          String hql="from Document where documenttype=:doc";
	          Query query = sesion.createQuery(hql);
	          query.setParameter("doc",documenttype);
	          return query.list();
	  }
	  
	  
	
	  
	  
	  
	  
	  public void updatecityAndpincodeBycontactnumber(String city,Long pincode,Long contactnumber)
	  {
		  SessionFactory ins = Utility.getInstance();
//	  		Configuration  cfg =new Configuration();
//	  		cfg.configure();
//	  		SessionFactory sessionFactory=cfg.buildSessionFactory();
	       	Session session= ins.openSession();
	       	Transaction transaction =session.beginTransaction();
	      	String hql="update Document set city=:c ,pincode=:pc where contactnumber=:cn";
	      	Query query =session.createQuery(hql);
	      	query.setParameter("c",city);
	      	query.setParameter("pc",pincode);
	      	query.setParameter("cn",contactnumber);
	      	int rowsUpdated=query.executeUpdate();
	      	transaction.commit();
	      	if(rowsUpdated>0)
	      {
	      	System.out.println("updated check");
	      }
	      else
	      {
	      	System.out.println("not updated  please");
	      }
	  } 
	  public void  deleteByFirstnameAndLastname(String firstname,String lastname)
	     {
		  SessionFactory ins = Utility.getInstance();
//	    	 	Configuration cfg = new Configuration();
//	    	 	cfg.configure();
//	    	 	SessionFactory sessionFactory = cfg.buildSessionFactory();
	    	 	Session session = ins.openSession();
	    	    Transaction transaction = session.beginTransaction();
	    	    String hql="delete Document where firstname=:fn And lastname=:ln";
	    	    
	    	    Query query = session.createQuery(hql);
	    	    query.setParameter("fn" , firstname);
	    	    query.setParameter( "ln", lastname);
	    	 
	    	    int rowUpdate = query.executeUpdate();
	    	    if(rowUpdate > 0) System.out.println("delete success");
	    	    else System.out.println("delete failed");
	    	    transaction.commit();
	     }

     public void verifyDocumentById(Long Id)
     {
    	 SessionFactory ins = Utility.getInstance();
    	 Document document=getDocumentById(Id);
    	 if(document!=null)
    	 {
    		 if(document.isDocumentverified())
    		 {
    			 System.out.println("Document is Verified");
    		 }
    		 else
    		 {
    			 
//    			document.setDocumentverified(true);
//    			 
//    			Configuration cfg = new Configuration();
// 	    	 	cfg.configure();
// 	    	 	SessionFactory sessionFactory = cfg.buildSessionFactory();
 	    	 	Session session = ins.openSession();
 	    	    Transaction transaction = session.beginTransaction();
 	    	    session.merge(document);
 	    	    transaction.commit();
    			 
    			 System.out.println("Document is  verifed succesfully");
    		 }
    	 }
    	 else {
    			 System.out.println("Please Enter valid ID");
    			 
    		 }
    	 }
     }	  	  
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
